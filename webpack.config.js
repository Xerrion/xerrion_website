const path = require("path");
const webpack = require("webpack");
const BundleTracker = require("webpack-bundle-tracker");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== "production";
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

let cleanPath = ["assets/dist"];

let cleanOptions = {
  verbose: true
};

module.exports = {
  mode: "production",
  optimization: {
    minimizer: [new OptimizeCSSAssetsPlugin({})]
  },
  entry: {
    main: [path.join(__dirname, "assets/src/js/index.js")]
  },
  output: {
    path: path.join(__dirname, "assets/dist"),
    filename: "[name]-[hash].js"
  },
  plugins: [
    new BundleTracker({
      path: __dirname,
      filename: "webpack-stats.json"
    }),
    new MiniCssExtractPlugin({
      filename: "[name]-[hash].css",
      chunkFilename: "[id].[hash].css"
    }),
    new CleanWebpackPlugin(cleanPath, cleanOptions),
  ],
  module: {
    rules: [
      {
        test: /\.s[c|a]ss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      }
    ]
  }
};
